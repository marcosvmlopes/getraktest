#language: pt
Funcionalidade: Login
    Sendo um usuário
    Posso acessar a plataforma https://teste-web.getrak.com.br/dev3
    Para poder ter acesso as funcionalidades de rastreamento veicular

  Cenario: Usuario faz login com usuario administrador
    Dado que eu acesso a aplicação
    Quando informo o usuario "dev3"
    E a senha "dev3"
    E seleciono entrar
    Então vejo a tela inicial do sistema

  Esquema do Cenario: Tentativa de login
    Dado que eu acesso a aplicação
    Quando informo o usuario <usuario>
    E a senha <senha>
    E seleciono entrar
    Então vejo a mensagem <msg>

    Exemplos: 
      | usuario                    | senha       | msg                         |
      | "avaliacao_qa_"            | " "         | "Usuário ou senha inválida" |
      | " "                        | "123456789" | "Usuário ou senha inválida" |
      | " "                        | " "         | "Usuário ou senha inválida" |
      | "marcos.vmlopes@gmail.com" | "12345678 " | "Usuário ou senha inválida" |
