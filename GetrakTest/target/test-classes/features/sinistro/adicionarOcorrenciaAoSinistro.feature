#language: pt

Funcionalidade: Adicionar ocorrência ao Sinistro
Sendo um usuário do sistema getrak
Posso entrar em um sinistro em aberto
Para poder incluir ocorrências referente a este sinistro em específico.

Critério de aceitação:

    Todas as informações presentes na página 8 do anexo sugestoes_V5_sinistro.pdf devem estar presentes
    Ao selecionar a barra Adicionar ocorrência devem surgir as opções para preenchimento
    Categoria, deve-se registrar o timestamp que foi feita a inclusão:
        Sinistro notificado a central
        Tentativa de acionamento do pronta-resposta
        Pronta-resposta acionado
        Registrado o BO na policia
        Apoio a caminho
        Apoio chega ao local
        Veículo encontrado
        Solicitada viatura para recuperação do veículo
        Polícia chega ao local
        modulo encontrado (desinstalado)
        Faz preservação no local
        Apoio inicia patrulhamento
        Pronta-resposta notifica o excedimento das condições de busca
        Outro
    Deve incluir um campo para comentário logo após a seleção de categoria
    O endereço da ocorrência pode ser digitado ou selecionado no mapa
    A data e hora vem por default o timestamp atual, ou seja, o horário atual da adição da ocorrência mas podendo ser alterado
    Deve conter o botão adicionar, o qual salvará as informações da ocorrência no sinistro
    Deve conter o botão Cancelar caso o usuário desista de adicionar a ocorrência
    Ao concluir o usuário deve ser redirecionado a tela anterior do Histórico do sinistro
    Não é permitido abrir ocorrência antes do horário de abertura do Sinistro

Documentos Anexos
         sugestoes_V5_sinistro.pdf
 
Informações para os teste:
     Link de acesso
         https://teste-web.getrak.com.br/dev3
 

     Usuário administrador central Getrak
         login: dev3
         senha:dev3
 
     Usuário Operador central Getrak
         login: operador
         senha: 123

Contexto:
	Dado que eu tenha um veiculo cadastrado no sistema e ativo
		E que eu tenha um veiculo cadastrado no sistema e ativo

@do
Cenário: Inserir uma ocorrência valido
     Dado que eu tenha um sinistro em aberto
     Quando eu adiciono ocorrencias valida para este veiculo informando todos os dados necessarios
     Então o usuário e redirecionado a tela anterior do Histórico do sinistro com a  visualização da ocorrência.
     
@do     
Cenário: Validando campos obrigatórios
     Dado que eu tenha um sinistro em aberto
     Quando eu adiciono ocorrencias porem não os campos obrigatorios
     Então o sistema informar quais campos não pode ser enviados em branco    
     
       