package br.sb.getrak.steps;

import static br.sb.getrak.core.DriverFactory.killDriver;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import br.sb.getrak.core.DriverFactory;
import br.sb.getrak.core.Propriedades;
import br.sb.getrak.pages.LoginPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Base {
	
	private LoginPage page = new LoginPage();
	
	@Rule
	public TestName testname = new TestName();
	
	@Before("@do")
	public void inicializa() {		
		page.acessarTelaInicial();
		page.logar("dev3", "dev3");
	}
	
	@After
	public void finaliza() throws IOException {

		TakesScreenshot ss = (TakesScreenshot) DriverFactory.getDriver();
		File arquivo = ss.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo, new File(
				"target" + File.separator + "screenshots" + File.separator + testname.getMethodName() + ".jpg"));

		if (Propriedades.FECHAR_BROWSER) {
			killDriver();
		}
	}

}
