package br.sb.getrak.steps;

import org.junit.Assert;

import br.sb.getrak.pages.SinistroPage;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class Sinistro {

	SinistroPage sinistro = new SinistroPage();

	@Dado("^que eu tenha um veiculo cadastrado no sistema e ativo$")
	public void queEuTenhaUmVeiculoCadastradoNoSistemaEAtivo() throws Throwable {
		sinistro.DadosBancoSinistro();
	}

	@Dado("^que eu acesse a funcionalidade de sinistro$")
	public void queEuAcesseAFuncionalidadeDeSinistro() throws Throwable {
		sinistro.acessarTelaSinistro();
	}

	@Quando("^acesso o formulário de cadastro de sinistro, preenchendo todos os dados e salvo$")
	public void acessoOFormulárioDeCadastroDeSinistroPreenchendoTodosOsDadosESalvo() throws Throwable {
		sinistro.abrirSinistro("SNC-0001", "Roubo", "Luiz Roxa", "31994637769", "texto com codigo refatorado",
				"rua itau, betim");
	}

	@Entao("^consigo vizualisar o card do sinistro aberto$")
	public void consigoVizualisarOCardDoSinistroAberto() throws Throwable {
		Assert.assertEquals("SNC-0001", sinistro.obterDadosDoCard("SNC-0001"));
		sinistro.RemoverDadosSinistro();
	}

	@Quando("^acesso o formulario de sinistro e tento salvar sem preencher$")
	public void acessoOFormularioDeSinistroETentoSalvarSemPreencher() throws Throwable {
		sinistro.acessarFormSinistro();
		sinistro.enviar();
	}

	@Entao("^o sistema informar quais campos não pode ser enviados em branco$")
	public void oSistemaInformarQuaisCamposNãoPodeSerEnviadosEmBranco() throws Throwable {
		Assert.assertEquals("Campo obrigatório", sinistro.obterTextoCamposObrigatorio());
		sinistro.RemoverDadosSinistro();
	}

	@Quando("^preencho todos os campos do formulário informando a data e hora quarenta e oito horas antes da data vigente$")
	public void preenchoTodosOsCamposDoFormulárioInformandoADataEHoraQuarentaEOitoHorasAntesDaDataVigente()
			throws Throwable {
		sinistro.acessarFormSinistro();
		sinistro.abrirSinistroPassado("SNC-0001", "Roubo", "Luiz Roxa", "31994637769", "texto com codigo refatorado",
				"rua itau, betim");
	}

	@Entao("^o sistema não permite a selecao do dia deixando o campo em branco$")
	public void oSistemaNãoPermiteASelecaoDoDiaDeixandoOCampoEmBranco() throws Throwable {
		Assert.assertEquals("Campo obrigatório", sinistro.obterTextoCamposObrigatorio());
		sinistro.RemoverDadosSinistro();
	}

	@Dado("^que eu tenha um sinistro em aberto$")
	public void queEuTenhaUmSinistroEmAberto() throws Throwable {
		sinistro.acessarTelaSinistro();
		sinistro.abrirSinistro("SNC-0001", "Roubo", "Luiz Roxa", "31994637769", "texto com codigo refatorado",
				"rua itau betim");
		sinistro.acessarTelaSinistro();
	}

	@Quando("^solicito o fechamento do sinistro executando todo o processo$")
	public void solicitoOFechamentoDoSinistroExecutandoTodoOProcesso() throws Throwable {
		sinistro.fecharSinistro("SNC-0001", "Veículo encontrado", "Média", "rua itau, betim", "854213652",
				"texto com codigo refatorado");
	}

	@Entao("^não consigo vizualisar o card do sinistro fechado$")
	public void nãoConsigoVizualisarOCardDoSinistroFechado() throws Throwable {
		Assert.assertEquals("Sinistro fechado com sucesso!", sinistro.obterTextoResposta());
		sinistro.RemoverDadosSinistro();
	}

	@Quando("^eu adiciono ocorrencias valida para este veiculo informando todos os dados necessarios$")
	public void eu_adiciono_ocorrencias_valida_para_este_veiculo_informando_todos_os_dados_necessarios()
			throws Throwable {
		sinistro.adicionarOcorrenciaNoSinistro("SNC-0001", "Tentativa de acionamento do pronta-resposta", 0,
				"enderecoOcorrencia", "rua norte, betim", "descricao", "texto com codigo refatorado");
	}

	@Entao("^o usuário e redirecionado a tela anterior do Histórico do sinistro com a  visualização da ocorrência\\.$")
	public void o_usuário_e_redirecionado_a_tela_anterior_do_Histórico_do_sinistro_com_a_visualização_da_ocorrência()
			throws Throwable {
		Assert.assertEquals("Ocorrencia cadastrada com sucesso!", sinistro.obterTextoResposta());
		Assert.assertEquals("Rua Norte Amazonas Betim-MG 32685-046 Brasil",
				sinistro.obterDadosDaOcorrencia("Rua Norte Amazonas"));
		sinistro.RemoverDadosSinistro();
	}
	
	@Quando("^eu adiciono ocorrencias porem não os campos obrigatorios$")
	public void euAdicionoOcorrenciasPoremNãoOsCamposObrigatorios() throws Throwable {
		sinistro.acessarDetalheSinistro("SNC-0001");
		sinistro.botaoAdicionarOcorrencia();
		sinistro.salvarOcorrencia();
	}
	
	@Quando("^acesso o formulário de cadastro de sinistro, preenchendo os dados, passo uma placa não rastreada e salvo$")
	public void acessoOFormulárioDeCadastroDeSinistroPreenchendoOsDadosPassoUmaPlacaNãoRastreadaESalvo() throws Throwable {
		sinistro.abrirSinistro("sabbath", "Roubo", "Luiz Roxa", "31994637769", "texto com codigo refatorado",
				"rua itau, betim");
	}

	@Entao("^consigo vizualisar o card do sinistro aberto da placa não rastreada$")
	public void consigoVizualisarOCardDoSinistroAbertoDaPlacaNãoRastreada() throws Throwable {
		Assert.assertEquals("sabbath", sinistro.obterDadosDoCard("sabbath"));
		sinistro.RemoverDadosSinistro();
	}	
}
