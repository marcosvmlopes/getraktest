package br.sb.getrak.steps;

import org.junit.Assert;

import br.sb.getrak.pages.LoginPage;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class Login {
	
	 LoginPage page = new LoginPage();
	
	@Dado("^que eu acesso a aplicação$")
	public void queEuAcessoAAplicação() throws Throwable {
		page.acessarTelaInicial();
	}

	@Quando("^informo o usuario \"([^\"]*)\"$")
	public void informoOUsuario(String usuario) throws Throwable {
		page.setUser(usuario);
	}

	@Quando("^a senha \"([^\"]*)\"$")
	public void aSenha(String senha) throws Throwable {
		page.setSenha(senha);
	}

	@Quando("^seleciono entrar$")
	public void selecionoEntrar() throws Throwable {
		page.entrar();
	}

	@Então("^vejo a tela inicial do sistema$")
	public void vejoATelaInicialDoSistema() throws Throwable {
		Assert.assertEquals("dev3", page.obterTextoUsuario());
		page.sair();
	}

	@Então("^vejo a mensagem \"([^\"]*)\"$")
	public void vejoAMensagem(String mensagemErro) throws Throwable {
		Assert.assertEquals(mensagemErro, page.obterMensagemFalha());
	}
}
