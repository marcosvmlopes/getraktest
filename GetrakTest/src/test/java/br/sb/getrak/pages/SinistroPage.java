package br.sb.getrak.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.sb.getrak.conexao.ConexaoMysql;
import br.sb.getrak.core.BasePage;
import br.sb.getrak.core.Data;
import br.sb.getrak.core.DriverFactory;

public class SinistroPage extends BasePage {

	private int resultado;
	private String dataAgora;
	private String horas;
	private WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 25);

	public void DadosBancoSinistro() {
		// Cadastrando veiculo no banco
		// conexão

		ConexaoMysql conexao = new ConexaoMysql();
		conexao.conectar();

		// insert equipamento resultado = conexao.
		resultado = conexao.insertSQL(
				"INSERT INTO `veiculos`(`modulo`, `datacad`, `sistema`, `lon`, `lat`, `status`, `datastatus`,"
						+ "`timezone`, `timestamp`, `saidas`, `celmodulo`, `vel`, `versao`, `saida1`, `saida2`, `saida3`, `saida4`,"
						+ "`entrada1`, `entrada2`, `entrada3`, `entrada4`, `equipamento`, `panico`, `antena_gps`, `alimentacao`,"
						+ "`num_chip`, `ativo`, `modo_entradas`, `hodoini`, `hodoatual`, `horimetro`, `consumo_comb`, `operadora`,"
						+ "`apn`, `comodato`) VALUES ('CE542677875',NULL,'dev3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,"
						+ "'Continenta','Saida 1','Saida 2','Saida 3','Saida 4','Entrada 1','Entrada 2','Entrada 3','Entrada 4',"
						+ "'542677875','0','1','1','564864548645378452','Y','0000','0',NULL,NULL,'0','oi','oi.com.br','N')");

		// insert veiculo resultado = conexao.
		resultado = conexao
				.insertSQL("INSERT INTO `veiculo`(`sistema`, `modulo`, `calculo`, `ativo`, `placa`, `apelido`,"
						+ "`descricao`, `icone`, `chassi`, `renavam`, `marca`, `modelo`, `cor`, `ano`, `anomodelo`, `combustivel`,"
						+ "`timezone`, `contato`, `tel_contato`, `cod_cliente1`, `cod_cliente2`, `cod_cliente3`, `pergunta_panico`,"
						+ "`resposta_panico`, `saida1`, `saida2`, `saida3`, `saida4`, `entrada1`, `entrada2`, `entrada3`, `entrada4`,"
						+ "`codigo`, `modo_entradas`, `obs`, `consumo_comb`) VALUES ('dev3','CE542677875','7','Y','SNC-0001',NULL,'',"
						+ "'50','','','','','','','','G','America/Sao_Paulo','TesteSelenium','',NULL,NULL,NULL,'','','Saída 1',"
						+ "'Saída 2','Saída 3','Saída 4','Entrada 1','Entrada 2','Entrada 3','Entrada 4','','0000','','0')");
		System.out.println("Resultado inclusao, id: " + resultado);
	}

	public void RemoverDadosSinistro() {
		// Excluindo dados do teste
		// conexão
		ConexaoMysql conexao = new ConexaoMysql();
		conexao.conectar();

		// Excluir sinistro
		resultado = conexao.insertSQL(
				"DELETE FROM `sinistro` WHERE `sistema` = 'dev3' AND `descricao` = 'Sinistro aberto pelo selenium automaticamente'");

		// Exclui ocorrencias sinistro
		resultado = conexao.insertSQL(
				"DELETE FROM `sinistro_evento` WHERE `descricao` = 'Sinistro aberto pelo selenium automaticamente'");

		// excluir veiculo
		resultado = conexao.insertSQL("DELETE FROM `veiculo` WHERE `sistema` = 'dev3' AND `placa` = 'SNC-0001'");

		// excluir equipamento
		resultado = conexao.insertSQL("DELETE FROM `veiculos` WHERE `sistema` = 'dev3' AND `modulo` = 'CE542677875'");

		// excluir dados sinistro
		resultado = conexao.insertSQL("DELETE FROM `sinistro` WHERE `nome_contato` = 'Luiz Roxa'");

		// excluir eventos do sinistro
		resultado = conexao
				.insertSQL("DELETE FROM `sinistro_evento` WHERE `descricao` = 'texto com codigo refatorado'");
	}

	public void acessarTelaSinistro() {
		clicarLink("Sinistro");
	}

	public void acessarFormSinistro() {
		clicarLink("ABRIR UM SINISTRO");
	}

	public void selecionarVeiculo(String placa) {
		escrever("veiculo", placa);
		clicarBotao(By.className("auto-complete-option"));
	}

	public void selecionarComboSinistro(String situacao) {
		clicarBotao(By.xpath("//label[contains(.,'" + situacao + "')]"));
	}

	public void selecionarDataHoje() {
		// pegando o dia atual
		dataAgora = Data.getCurrentDay();

		// Abrindo o calendarios
		clicarBotao(By.xpath("//form//input[@name='dataAcontecimento']"));
		selecionarDate(dataAgora);
	}

	public void selecionarDataPassada() {
		// pegando o dia atual
		dataAgora = "20";

		// Abrindo o calendarios
		clicarBotao(By.xpath("//form//input[@name='dataAcontecimento']"));
		selecionarDate(dataAgora);
	}

	public void selecionarDataOcorrencia() {
		// pegando o dia atual
		dataAgora = Data.getCurrentDay();

		// Abrindo o calendarios e selecionado data de hoje
		clicarBotao("datepicker-trigger");
		selecionarDate(dataAgora);
	}

	public void horaOcorrencia(int hora) {
		// pegando a hora
		horas = Data.getCurrentHourManipulate(hora, "");
		escrever("hora", horas);
	}

	public void escreverCampoHora(int hora) {
		// pegando a hora
		horas = Data.getCurrentHourManipulate(hora, "-");
		escrever("horaAcontecimento", horas);
	}

	public void inserirContato(String contato) {
		escrever("nomeContato", contato);
	}

	public void inserirTelefoneContato(String telefone) {
		escrever("telefone", telefone);
	}

	public void inserirDescricao(String descricao) {
		escrever("descricaoOcorrido", descricao);
	}

	public void inserirEndereco(String idCampo, String local) {
		escrever(idCampo, local);
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//*[@class='auto-complete-option']/*[contains(.,'Betim-MG')]")));
		clicarBotao(By.className("auto-complete-option"));
	}

	public void enviar() {
		clicarBotao("enviar");
	}

	public void salvarOcorrencia() {
		clicarBotao(By.xpath("//button[@class='btn btn-success']/span[text()='ADICIONAR']"));
	}

	public void fechar() {
		clicarBotao(By.xpath("//Button[@class='btn btn-verde btn-link']"));
	}

	public String obterDadosDoCard(String placa) {
		String card = obterTexto(By.xpath("//*[@id='listaDeSinistros']/*[@class='sinistro row']/*"
				+ "[@class='cabecalhoDoSinistro row']/*[@class='col-md-4 col-lg-5 identificacao']/h3[text()='" + placa
				+ "']"));
		return card;
	}

	public String obterDadosDaOcorrencia(String endereco) {
		wait.until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//*[@class='content']/p[contains(.,'" + endereco + "')]")));
		String card = obterTexto(By.xpath("//*[@class='content']/p[contains(.,'" + endereco + "')]"));
		return card;
	}

	public String obterTextoCamposObrigatorio() {
		String texto = obterTexto(By.xpath("//span[contains(.,'Campo obrigatório')]"));

		return texto;
	}

	public String obterTextoResposta() {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='notification-title']")));
		String texto = obterTexto(By.xpath("//div[@class='notification-title']"));
		return texto;
	}

	public void acessarDetalheSinistro(String placa) {
		clicarBotao(By.xpath("//*[@id='listaDeSinistros']/*[@class='sinistro row']/*"
				+ "[@class='cabecalhoDoSinistro row']/*[@class='col-md-4 col-lg-5 identificacao']/h3[text()='" + placa
				+ "']"));
	}

	public void acessarDetalheOcorrenciaPorEndereco(String endereco) {
		clicarBotao(By.xpath("//*[@class='content']/p[contains(.,'" + endereco + "')]"));
	}

	public void botaoFecharSinistro() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//strong[contains(.,'FECHAR SINISTRO')]")));
		clicarBotao(By.xpath("//strong[contains(.,'FECHAR SINISTRO')]"));
	}

	public void botaoAdicionarOcorrencia() {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//strong[contains(.,'ADICIONAR OCORRÊNCIA')]")));
		clicarBotao(By.xpath("//strong[contains(.,'ADICIONAR OCORRÊNCIA')]"));
	}

	public void selecionaMotivoFechamento(String motivo) {
		selecionarComboViaXpath(By.xpath("//select[@name='motivo']"), motivo);
	}

	public void selecionaCategoria(String categoria) {
		selecionarCombo("categoria", categoria);
	}

	public void selecionarAvaria(String avaria) {
		selecionarComboViaXpath(By.xpath("//select[@name='0']"), avaria);
	}

	public void setKmProntaResposta(String km) {
		escrever("km_veiculo", km);
	}

	public void setComentario(String nomeCampo, String comentario) {
		escrever(By.xpath("//textarea[@name='" + nomeCampo + "']"), comentario);

	}

	public void abrirSinistro(String placa, String situacao, String contato, String telefone, String descricao,
			String local) {
		acessarFormSinistro();
		selecionarVeiculo(placa);
		selecionarComboSinistro(situacao);
		selecionarDataHoje();
		escreverCampoHora(7);
		inserirContato(contato);
		inserirTelefoneContato(telefone);
		inserirDescricao(descricao);
		inserirEndereco("local", local);
		enviar();
	}

	public void abrirSinistroPassado(String placa, String situacao, String contato, String telefone, String descricao,
			String local) {
		acessarFormSinistro();
		selecionarVeiculo(placa);
		selecionarComboSinistro(situacao);
		selecionarDataPassada();
		escreverCampoHora(8);
		inserirContato(contato);
		inserirTelefoneContato(telefone);
		inserirDescricao(descricao);
		inserirEndereco("local", local);
		enviar();
	}

	public void fecharSinistro(String placa, String motivo, String avaria, String local, String km, String comentario) {
		acessarDetalheSinistro(placa);
		botaoFecharSinistro();
		selecionaMotivoFechamento(motivo);
		selecionarAvaria(avaria);
		inserirEndereco("local", local);
		setKmProntaResposta(km);
		setComentario("comentario", comentario);
		enviar();
		fechar();
	}

	public void adicionarOcorrenciaNoSinistro(String placa, String categoria, int hora, String idCampo, String local,
			String nomeCampo, String comentario) {
		acessarDetalheSinistro(placa);
		botaoAdicionarOcorrencia();
		selecionaCategoria(categoria);
		selecionarDataOcorrencia();
		horaOcorrencia(hora);
		inserirEndereco(idCampo, local);
		setComentario(nomeCampo, comentario);
		salvarOcorrencia();
	}

}
