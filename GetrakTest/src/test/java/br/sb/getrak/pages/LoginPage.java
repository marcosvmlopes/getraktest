package br.sb.getrak.pages;

import org.openqa.selenium.By;

import br.sb.getrak.core.BasePage;
import br.sb.getrak.core.DriverFactory;

public class LoginPage extends BasePage {
	
	
	public void acessarTelaInicial() {
		DriverFactory.getDriver().get("https://teste-web.getrak.com.br/dev3");
	}

	public void setUser(String user) {
		escrever("login", user);
	}

	public void setSenha(String senha) {
		escrever("senha", senha);
	}

	public void entrar() {
		clicarBotaoPorType("submit");
	}

	public String obterTextoUsuario() {
		return obterTexto(By.xpath("//a[@id='user-area']"));
	}

	public String obterMensagemFalha() {
		return obterTexto(By.xpath("//span[@class='erro']"));
	}

	public void sair() {
		clicarBotao(By.xpath("//a[@class='sair']"));
	}

	public void logar(String user, String senha) {
		setUser(user);
		setSenha(senha);
		entrar();
	}

}