package br.sb.getrak.core;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import br.sb.getrak.core.Propriedades.TipoExecucao;
import br.sb.getrak.core.Propriedades.TipoNuvem;

public class DriverFactory {
	// private static WebDriver driver;
	private static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<WebDriver>() {
		@Override
		protected synchronized WebDriver initialValue() {
			return initDriver();
		}
	};

	private DriverFactory() {
	}

	public static WebDriver getDriver() {
		return threadDriver.get();
	}

	@SuppressWarnings("deprecation")
	public static WebDriver initDriver() {

		WebDriver driver = null;

		if (Propriedades.TIPO_EXECUCAO == TipoExecucao.LOCAL) {
			switch (Propriedades.BROWSER) {
			case FIREFOX:
				// configurações do capabilities do firefox
				FirefoxProfile profile = new FirefoxProfile();
				profile.setPreference("permissions.default.desktop-notification", 1); // Desativar as notificações do navegador
				DesiredCapabilities cap = DesiredCapabilities.firefox(); 
				cap.setCapability(FirefoxDriver.PROFILE, profile);
				
				// subindo o driver com os capabilites configurados
				System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/Windows/geckodriver.exe");
				driver = new FirefoxDriver(cap);
				break;
			case CHROME:
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-notifications"); // Desativar as notificações do navegador
				System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/Windows/chromedriver.exe");
				driver = new ChromeDriver(options);
				break;
			case SAFARI:
				driver = new SafariDriver();
				break;
			}
		}
		if (Propriedades.TIPO_EXECUCAO == TipoExecucao.GRID) {
			DesiredCapabilities cap = null;
			switch (Propriedades.BROWSER) {
			case FIREFOX:
				cap = DesiredCapabilities.firefox();
				break;
			case CHROME:
				cap = DesiredCapabilities.chrome();
				break;
			case SAFARI:
				cap = DesiredCapabilities.safari();
				break;
			}

			try {
				driver = new RemoteWebDriver(new URL(" http://localhost:4444/wd/hub"), cap);
			} catch (MalformedURLException e) {
				System.err.println("Falha na conecção do GRID");
				e.printStackTrace();
			}

		}
		if (Propriedades.TIPO_EXECUCAO == TipoExecucao.NUVEM) {
			DesiredCapabilities cap = null;
			switch (Propriedades.BROWSER) {
			case FIREFOX:
				if (Propriedades.TIPO_NUVEM == TipoNuvem.SAUCELABS) {
					cap = DesiredCapabilities.firefox();
					cap.setCapability("platform", "Windows 7");
					cap.setCapability("version", "64.0");
				}
				if (Propriedades.TIPO_NUVEM == TipoNuvem.BROWSERSTACK) {
					cap = new DesiredCapabilities();
					cap.setCapability("browser", "Firefox");
					cap.setCapability("browser_version", "64.0");
					cap.setCapability("os", "Windows");
					cap.setCapability("os_version", "7");
					cap.setCapability("resolution", "1280x800");
				}
				break;
			case CHROME:
				if (Propriedades.TIPO_NUVEM == TipoNuvem.SAUCELABS) {
					cap = DesiredCapabilities.chrome();
					cap.setCapability("platform", "Windows 7");
					cap.setCapability("version", "71.0");
				}
				if (Propriedades.TIPO_NUVEM == TipoNuvem.BROWSERSTACK) {
					ChromeOptions options = new ChromeOptions();
					cap = new DesiredCapabilities();
					cap.setCapability("browser", "Chrome");
					cap.setCapability("browser_version", "71.0");
					cap.setCapability("os", "Windows");
					cap.setCapability("os_version", "7");
					cap.setCapability("resolution", "1280x800");
					cap.setCapability("browserstack.debug", "true");
					cap.setCapability(ChromeOptions.CAPABILITY, options);
				}
				break;
			case SAFARI:
				cap = new DesiredCapabilities();
				cap.setCapability("os", "OS X");
				cap.setCapability("os_version", "Mojave");
				cap.setCapability("browser", "Safari");
				cap.setCapability("browser_version", "12.0");
				cap.setCapability("resolution", "1920x1080");
				cap.setCapability("browserstack.local", "false");
				cap.setCapability("browserstack.selenium_version", "3.14.0");
				cap.setCapability("browserstack.safari.enablePopups", "true");
				break;
			}
			if (Propriedades.TIPO_NUVEM == TipoNuvem.SAUCELABS) {
				try {
					driver = new RemoteWebDriver(new URL(
							"http://testesaba:7fa2ab8f-85e2-40f0-a2da-d05351b4ccfb@ondemand.saucelabs.com:80/wd/hub"),
							cap);
				} catch (MalformedURLException e) {
					System.err.println("Falha na conecção do GRID");
					e.printStackTrace();
				}
			}
			if (Propriedades.TIPO_NUVEM == TipoNuvem.BROWSERSTACK) {
				try {
					driver = new RemoteWebDriver(
							new URL("https://testelopes2:mhyLbAY8Yr7hZ1th3qrt@hub-cloud.browserstack.com/wd/hub"), cap);
				} catch (MalformedURLException e) {
					System.err.println("Falha na conecção do GRID");
					e.printStackTrace();
				}
			}

		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return driver;
	}

	public static void killDriver() {
		WebDriver driver = getDriver();
		if (driver != null) {
			driver.quit();
			driver = null;
		}
		if (threadDriver != null) {
			threadDriver.remove();
		}
	}
}
