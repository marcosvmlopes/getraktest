package br.sb.getrak.core;

public class Propriedades {
	
	public static boolean FECHAR_BROWSER = true;
	
	public static Browsers BROWSER = Browsers.CHROME;
	
	public static TipoExecucao TIPO_EXECUCAO = TipoExecucao.LOCAL;
	
	public static TipoNuvem TIPO_NUVEM = TipoNuvem.BROWSERSTACK;
	
	public enum Browsers {
		CHROME,
		FIREFOX,	
		SAFARI
	}
	
	public enum TipoExecucao {
		LOCAL,
		GRID,
		NUVEM
	}
	
	public enum TipoNuvem {
		SAUCELABS,
		BROWSERSTACK
	}

}
