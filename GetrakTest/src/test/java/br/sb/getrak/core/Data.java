package br.sb.getrak.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Data {

	// pegando a data atual
	public static String getCurrentDay() {
		// Creando o objeto Calendario
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

		// Pegando o dia atual como um numero inteiro
		int todayInt = calendar.get(Calendar.DAY_OF_MONTH);

		// Convertendo o numero inteiro para string
		String todayStr = Integer.toString(todayInt);

		return todayStr;
	}

	// Pegando a hora atual menos quantidade de horas passada
	public static String getCurrentHourManipulate(int valor, String acao) {
		// Creando o objeto Calendario
		Calendar calendar = Calendar.getInstance();

		// Pegando o hora atual menos horas passada
		calendar.setTime(new Date());
		if (acao == "+") {
			calendar.add(Calendar.HOUR_OF_DAY, valor);

		} else if (acao == "-") {
			calendar.add(Calendar.HOUR_OF_DAY, -valor);
		} else {
			System.out.println("Valor de ação incorreto, sera ultilizado a hora atual.");
		}

		java.util.Date date = calendar.getTime();
		// Formatando a hora
		DateFormat todayHour = new SimpleDateFormat("HH:mm:ss");

		// Convertendo o numero inteiro para string
		String formattedDate = todayHour.format(date);

		return formattedDate;
	}

}
