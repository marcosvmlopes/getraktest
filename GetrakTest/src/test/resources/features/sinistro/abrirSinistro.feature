#language: pt

Funcionalidade: Abrir Sinistro
Sendo um usuário do sistema getrak
Posso acessar o "Sinistro" no menu principal
Para poder abrir um sinistro quando for o caso

Critério de aceitação:

    Ao clicar no botão "Abrir um sinistro" deve me aparecer os campos para preenchimento na área de informação e interação
    Os campos obrigatórios devem ser informados, vide pág 2 do anexo sugestoes_V5_sinistro.pdf
    A opção "Do que se trata o sinistro" não deve ter opção default
    A hora de abertura do sinistro deve ser armazenada (não é visível para o usuário)
    Os campos "Estado" e "Cidade" devem ser preenchidos automaticamente de acordo com a ultima posição válida do modulo
    O usuário deve ser alertado que é permitido selecionar o endereço pelo mapa
    Caso o endereço for pintado pelo mapa o campo "Onde ocorreu o sinistro" deve ser setado com o endereço do pino no mapa
    Caso o usuário clicar no botão "ABRIR" deve verificar se os campos obrigatórios foram preenchidos
        se sim: todas as informações devem ser salvas
        se não: deve-se informar quais campos precisam ser preenchidos
    Caso clicar "Cancelar" todas as informações devem ser descartadas e a tela deve voltar a página inicial com os cards e o mapa
    Não devemos abrir sinistros antes das 24 horas anteriores a hora atual
    É permitido abrir somente um sinistro por veículo
 
Dados para os teste:
     Link de acesso
         https://teste-web.getrak.com.br/dev3
     Usuário administrador central Getrak
         login: dev3
         senha:dev3
     Usuário Operador central Getrak
         login: operador
         senha: 123

Contexto:
	Dado que eu tenha um veiculo cadastrado no sistema e ativo
				
@do
Cenário: Abrir um sinistro valido
     Dado que eu acesse a funcionalidade de sinistro
     Quando acesso o formulário de cadastro de sinistro, preenchendo todos os dados e salvo
     Então consigo vizualisar o card do sinistro aberto
     
@do
Cenário: Abrir um sinistro para placa não rastreada
     Dado que eu acesse a funcionalidade de sinistro
     Quando acesso o formulário de cadastro de sinistro, preenchendo os dados, passo uma placa não rastreada e salvo
     Então consigo vizualisar o card do sinistro aberto da placa não rastreada

@do   
Cenário: Validando campos obrigatórios
     Dado que eu acesse a funcionalidade de sinistro
     Quando acesso o formulario de sinistro e tento salvar sem preencher
     Então o sistema informar quais campos não pode ser enviados em branco    
     
@do 
Cenário: Abrir um sinistro valido 48 horas antes da data vigente
     Dado que eu acesse a funcionalidade de sinistro
     Quando preencho todos os campos do formulário informando a data e hora quarenta e oito horas antes da data vigente
     Então o sistema não permite a selecao do dia deixando o campo em branco
     