#language: pt

Funcionalidade: Fechar Sinistro
Sendo um usuário do sistema getrak
Posso acessar o "Sinistro" no menu principal
Para poder fechar o sinistro quando o mesmo for concluído

Critério de aceitação:

    Todas as informações presentes na página 10 do anexo sugestoes_V5_sinistro.pdf devem estar presentes
    Ao selecionar a barra "Fechar sinistro" deve-se estender e solicitar ao usuário o motivo do fechamento
    Motivo do fechamento
        Veículo encontrado
        Veículo não encontrado
        Falso positivo
    Usuário tem a opção de Fechar sinistro (armazena as informações e encerra) ou cancelar a operação (descarta as informações e volta para o histórico do sinistro)
    Uma área de texto com o motivo de fechamento será exibida para que seja informado detalhes sobre o desfecho do sinistro.


Documentos Anexos
         sugestoes_V5_sinistro.pdf
 
informações para os teste:
     Link de acesso
         https://teste-web.getrak.com.br/dev3
 

     Usuário administrador central Getrak
         login: dev3
         senha:dev3
 
     Usuário Operador central Getrak
         login: operador
         senha: 123

Contexto:
	Dado que eu tenha um veiculo cadastrado no sistema e ativo
	
@do
Cenário: Fechar um sinistro
     Dado que eu tenha um sinistro em aberto
     Quando solicito o fechamento do sinistro executando todo o processo
     Então não consigo vizualisar o card do sinistro fechado
 